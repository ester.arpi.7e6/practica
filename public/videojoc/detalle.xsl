<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <xsl:element name="section"><xsl:attribute name="class">VideojuegosSection</xsl:attribute>
        <xsl:for-each select="Videojuegos/Videojuego">       
        <xsl:element name="article">
            <xsl:attribute name="class">VideojuegosArticle</xsl:attribute>            
                <xsl:element name="img"><xsl:attribute name="src"><xsl:value-of select="InfoGeneral/Foto"/></xsl:attribute></xsl:element>
                <xsl:element name="h3"><xsl:value-of select="Nombre"/></xsl:element>
            <xsl:element name="div">    
                <xsl:element name="p"><xsl:value-of select="InfoGeneral/Descripcion"/></xsl:element>
                <xsl:element name="p"><xsl:element name="span">Idiomas: </xsl:element><xsl:value-of select="InfoGeneral/Idiomas"/></xsl:element>
                <xsl:element name="p"><xsl:element name="span">Desarrollador: </xsl:element><xsl:value-of select="InfoGeneral/Desarrollador"/></xsl:element>
                <xsl:element name="p"><xsl:element name="span">Distribuidor: </xsl:element><xsl:value-of select="InfoGeneral/Distribuidor"/></xsl:element>
            </xsl:element>       
        </xsl:element>
        </xsl:for-each>
    </xsl:element>
</xsl:template>
</xsl:stylesheet>