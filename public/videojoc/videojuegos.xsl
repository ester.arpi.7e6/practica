<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">

    <xsl:element name="section"><xsl:attribute name="class">VideojuegosSection</xsl:attribute><xsl:attribute name="id">Videojuegos</xsl:attribute>
        <xsl:element name="h2">Videojuegos</xsl:element>
        <xsl:for-each select="Videojuegos/Videojuego">       
        <xsl:element name="article">
            <xsl:attribute name="class">VideojuegosArticle</xsl:attribute>
            <xsl:element name="div">
                <xsl:element name="img"><xsl:attribute name="src"><xsl:value-of select="InfoGeneral/Foto"/></xsl:attribute></xsl:element>
                <xsl:element name="h3"><xsl:value-of select="Nombre"/></xsl:element>
                <xsl:element name="p"><xsl:value-of select="InfoGeneral/Descripcion"/></xsl:element>
            </xsl:element>
            <xsl:element name="div">
            <xsl:attribute name="class">VideojuegosButton</xsl:attribute>                
                <xsl:element name="a"><xsl:attribute name="href">5DETALL.html?id=<xsl:value-of select="@idVideoJuego"/></xsl:attribute>Más info</xsl:element>
            </xsl:element> 
        </xsl:element>
        </xsl:for-each>
    </xsl:element>
    </xsl:template>
</xsl:stylesheet>